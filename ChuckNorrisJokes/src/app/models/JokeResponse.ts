import { Joke } from './Joke';

export interface JokeResponse {
    total: number;
    result: Joke[];
}
