export interface Joke {

    id: string;
    icon_url: string;
    url: string;
    value: string;
}
