import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChuckService } from '../services/chuck.service';
import { Joke } from '../models/Joke';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  jokes: Joke[] = [];

  constructor(private chuckService: ChuckService) { }

  ngOnInit() {
    this.loadOneMoreJoke();
  }

  loadOneMoreJoke() {
    this.chuckService.getRandomJoke().subscribe((result: Joke) => {
      this.jokes.unshift(result);
    });
  }

  deleteOneJoke(id: string) {
    this.jokes = this.chuckService.deleteJoke(this.jokes, id);
  }
}
