import { Component, OnInit, OnDestroy } from '@angular/core';
import { Joke } from '../models/Joke';
import { ChuckService } from '../services/chuck.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  jokes: Joke[] = [];
  categories: string[] = [];

  constructor(private chuckService: ChuckService) { }

  ngOnInit() {
    this.chuckService.getCategories().subscribe((result: string[]) => {
      this.categories = result;
    });
  }

  onSelectCategory(category: string) {

    if (category === '0')  {
      return;
    }

    this.chuckService.getJokeByCategory(category).subscribe((result: Joke) => {
      this.jokes.unshift(result);
    });

  }

  deleteOneJoke(id: string) {
    this.jokes = this.chuckService.deleteJoke(this.jokes, id);
  }

}
