import { Component } from '@angular/core';
import { ChuckService } from '../services/chuck.service';
import { Joke } from '../models/Joke';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {

  jokes: Joke[] = [];
  query = '';

  constructor(private chuckService: ChuckService) { }

  onSearch() {
    this.chuckService.searchJoke(this.query).subscribe(data => {
      this.jokes = data.result.splice(0, 10);
    });
  }

  deleteOneJoke(id: string) {
    this.jokes = this.chuckService.deleteJoke(this.jokes, id);
  }
}
