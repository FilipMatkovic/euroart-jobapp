import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Joke } from '../models/Joke';
import { JokeResponse } from '../models/JokeResponse';

@Injectable({
  providedIn: 'root'
})
export class ChuckService {
  url = 'https://api.chucknorris.io/jokes';

  constructor(private http: HttpClient) { }

  getCategories(): Observable<string[]> {
    return this.http.get<string[]>('https://api.chucknorris.io/jokes/categories');
  }

  getRandomJoke(): Observable<Joke> {
    return this.http.get<Joke>('https://api.chucknorris.io/jokes/random');
  }

  getJokeByCategory(category: string): Observable<Joke> {
    return this.http.get<Joke>('https://api.chucknorris.io/jokes/random?category=' + category);
  }

  searchJoke(query: string): Observable<JokeResponse>  {
    return this.http.get<JokeResponse>('https://api.chucknorris.io/jokes/search?query=' + query);
  }

  deleteJoke(data: Joke[], id: string) {
    return data.filter(joke => {

      if (joke.id !== id) {
        return joke;
      }
    });
  }
}
